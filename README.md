# Alma Segment "Work In Progress"

Alma Segment is a Luso-Brazilian project that emerged to collect data from nanosatellites. Two universities joined forces to develop ground segments, capable of working together to receive telemetry data, and decode information.

## Authors and acknowledgment

People directly involved in the project:

- Júlio Santos from UBI (juliosantoswork@gmail.com)

- Jeremy Silva from UBI (jeremy.silva.1998@gmail.com)

- Henrique Alves from UBI (henriquedalveswork@gmail.com)

- Daniel Resende from UBI (daniresa01@gmail.com)

- João Braga from UFSJ (joaopedropolito@aluno.ufsj.edu.br)

Show your appreciation to those who have contributed to the project.

# Index

- [Alma Segment "Work in Progress"](#alma-segment-"work-in-progress")
    * [Authors and acknowledgment](#authors-and-acknowledgment)
    * [Index](#index)
    * [Hardware Required](#hardware-required)
        + [First Frame](#first-frame)
        + [Second Frame](#second-frame)
        + [Final Assembly](#final-assembly)
        + [Aditional Hardware](#aditional-hardware)
            - [UHF](#uhf)
            - [VHF](#vhf)
        + [Electrical Hardware](#electrical-hardware)
    * [Software](#software)
    * [Integrate with your tools](#integrate-with-your-tools)
    * [Collaborate with your team](#collaborate-with-your-team)
    * [Test and Deploy](#test-and-deploy)
- [Editing this README](#editing-this-readme)
    * [Suggestions for a good README](#suggestions-for-a-good-readme)
    * [Name](#name)
    * [Description](#description)
    * [Badges](#badges)
    * [Visuals](#visuals)
    * [Installation](#installation)
    * [Usage](#usage)
    * [Support](#support)
    * [Roadmap](#roadmap)
    * [Contributing](#contributing)
    * [License](#license)
    * [Project status](#project-status)

## Hardware Required

The Hardware is divided into 3 parts: **SatNOGS Rotor**, **Ground Station Portugal**, **Ground Station Brazil**. If you want to replicate just one of the setups you can choose whether to build a diretional ground segment (for this one you will need to have a rotor) or a omnidiretional ground segment.

To make it easy for you to get started with the project, here is a list of the necessary hardware:

**SatNOGS Rotor v.3.0.1** : You can look at the assembly guide [here](https://ohai.libre.space/project/satnogs-rotator-v3-mechanical-assembly/satnogs/), however we found some of the material are not in the guide and decided to do our own

### First Frame

- 4x T slot 20x20 L160
- 9x T slot 20x20 L102
- 20x Hidden corner connection
- 8x M4-20 screws + 8x M4 nuts
- 2x Ball Bearing 6008ZZ
- 2x 3D printed [ball_bearing_housing]
- 1x 3D printed [axis_side_sensor_holder]
- 1x 3D printed [axis_side]  
- 4x M4 slot 6 nuts + 4x M4-10 bolts
- 1x 3D printed [worm_gear] 
- 2x M5 nut
- 1x M5 washer
- 90mm M5 threaded rod
- 2 ball bearings 625ZZ
- 4x M4 nut + 4x M4-10 screws + 2x M5 washer
- 2x M4 slot6 nuts + 2x M4-10 bolts
- 2x M4 slot6 nuts, 2x M4-20 bolts
- 1x motor mount
- 1x 3D printed [axis_spacer_collar]
- 3x M3 nut + 3x M3-10 set screws 
- 1x 3D printed [endstop_mount] 
- 1x M3 nut + 1x M3-25 bolt
- 1x 3D printed [homing-ring]
- 1x NEMA 17 stepper motor
- 4x M3-6 + 4x M3 washers
- GT2 pulley 20 T 5mm bore + GT2 pulley 36 T 5mm bore
- 1x GT2 timing belt 158mm
- 1x 3D printed [modified_end_stop_touch]

### Second Frame

- 4x T slot 20x20 L160
- 9x T slot 20x20 L102
- 20x Hidden corner connection
- 8x M4-20 screws + 8x M4 nuts
- 2x Ball Bearing 6008ZZ
- 2x 3D printed [ball_bearing_housing]
- 1x 3D printed [axis_side_sensor_holder]
- 1x 3D printed [axis_side]  
- 4x M4 slot 6 nuts + 4x M4-10 bolts
- 1x 3D printed [worm_gear]
- 2x M5 nut
- 1x M5 washer
- 90mm M5 threaded rod
- 2 ball bearings 625ZZ
- 4x M4 nut + 4x M4-10 screws + 2x M5 washer
- 2x M4 slot6 nuts + 2x M4-10 bolts
- 2x M4 slot6 nuts, 2x M4-20 bolts
- 1x motor mount
- 1x 3D printed [axis_spacer_collar]
- 3x M3 nut + 3x M3-10 set screws
- 1x 3D printed [endstop_mount]
- 1x M3 nut + 1x M3-25 bolt
- 1x NEMA 17 stepper motor
- 4x M3-6 + 4x M3 washers
- GT2 pulley 20 T 5mm bore + GT2 pulley 36 T 5mm bore
- 1x GT2 timing belt 158mm
- 1x 3D printed [modified_end_stop_touch]


### Final Assembly

- Cilindrical Steel Tubes (horizontal 2m ⌀38mm)
- Cilindrical Steel Tubes (vertial 1.5m ⌀38mm)
- Foundation (Square (200x200mm) soldered to vertical tube and glued to ground; Vertical support mid way across the vertical tube) - Adapted to place of mounting
- Acrylic Enclosure (Measures found in Acrylic Box.pdf)
- 4x M4-10 bolts + 4x M4 T-slot nuts + 4x M4 washers
- 2m of PVC Angles (https://www.checkmate-direct.co.uk/easyfix-19mm-angle---12-x-244m---black-32679-p.asp)
    

For 3D printed pieces, look at: [satnogs-rotator v.3.0.1](https://gitlab.com/librespacefoundation/satnogs/satnogs-rotator/-/tree/v3.0.1/rotator_parts). The end-stop piece was designed by us, and can be found as [modified_end_stop_touch]

**Note**: To open ".fcstd" you will need FreeCAD, nevertheless we share with you all the 3D files in .STL.

### Adittional Hardware

_Portugal_

![img](doc/img/antennas.png)

#### UHF
- [Crossed-Yagi Antenna UHF 2x10 el](https://www.wimo.com/en/wx-7021)
- [Phase line f. 70cm-Cross-Yagi w. connectors(2xN-plug, 1xN-socket)](https://www.wimo.com/en/18052)
- [1x N-plug, 1x SMA-socket Connector](https://cablematic.com/pt/produtos/adaptador-sma-femean-macho-WG027/)
- [Coaxial cable SMA-plug, SMA-socket](https://cablematic.com/pt/produtos/hdf200-cabo-coaxial-sma-macho-para-1m-sma-femea-WE001/)
- [1x SMA-plug, 1x SMA-plug](https://www.data-alliance.net/sma-male-to-sma-male-cable-3-inch-4-in-6-in-8-in-10-in-12-in-14-in-16-inch/)
- [LNA WideBand 50-2500Mhz](https://www.passion-radio.com/sdr-accessory/lna-opa-805.html)
- [1x SMA-plug, 1x SMA-plug](https://www.data-alliance.net/sma-male-to-sma-male-cable-3-inch-4-in-6-in-8-in-10-in-12-in-14-in-16-inch/)
- [RTL-SDR](https://www.astroradio.com/p/receptor_rtlsdr_blog_r820t2_rtl2832u/)
- 1x LNA Box Enclosure (Found in FM Filter & LNA Enclosures)

#### VHF
- [Crossed-Yagi Antenna VHF 2x4 el](https://www.wimo.com/en/wx-208)
- [Phase line f. 2m-Cross-Yagi 2xN-plug, 1xN-socket](https://www.wimo.com/en/18051)
- [1x N-plug, SMA-socket connector](https://cablematic.com/pt/produtos/adaptador-sma-femean-macho-WG027/)
- [Coaxial cable SMA-plug, SMA-socket](https://cablematic.com/pt/produtos/hdf200-cabo-coaxial-sma-macho-para-1m-sma-femea-WE001/)
- [1x SMA-plug, 1x SMA-plug](https://www.data-alliance.net/sma-male-to-sma-male-cable-3-inch-4-in-6-in-8-in-10-in-12-in-14-in-16-inch/)
- [FM Blocker](https://www.nooelec.com/store/sdr/sdr-addons/rf-blocks/distill-fm-broadcast-fm-filter.html)
- [1x SMA-plug, 1x SMA-plug](https://www.data-alliance.net/sma-male-to-sma-male-cable-3-inch-4-in-6-in-8-in-10-in-12-in-14-in-16-inch/)
- [LNA WideBand 50-2500Mhz](https://www.passion-radio.com/sdr-accessory/lna-opa-805.html) 
- [1x SMA-plug, 1x SMA-plug](https://www.data-alliance.net/sma-male-to-sma-male-cable-3-inch-4-in-6-in-8-in-10-in-12-in-14-in-16-inch/)
- [RTL-SDR](https://www.astroradio.com/p/receptor_rtlsdr_blog_r820t2_rtl2832u/)
- 1x LNA Box Enclosure (Found in FM Filter & LNA Enclosures)
- 1x FM Filter Box Enclosure (Found in FM Filter & LNA Enclosures) 

**NOTE:** All the COTS appear in the list will be sorted by order of connection.

### Electrical Hardware

- [1x Ethernet cable](https://www.pcdiga.com/cabo-rede-gembird-utp-cat6-10m-cinza-pp6u-10mgclid=Cj0KCQiAybaRBhDtARIsAIEG3knHA7DdWgaZAa8PJqLCOsKop8SiRpkTRqmSzpKfHMxf-L9LNgMbuNIaAsZYEALw_wcB)
- 1x Power Cable (local supply store)
- [2x Electrical Plugs Female](https://eclats-antivols.fr/en/rca/11975-plug-220vac-6a-female-electric-plug-1-item-plug-220vac-16a-female-electric-plug-electric-female-plugs-plug-220vac-16a-female-e-5410329238582.html)
- [1x Enclosure Box](https://www.altinkaya.eu/heavy-duty-enclosures/254-se-260)
- [3x PG7 cable glands for antenna cables and ethernet cable](https://www.ampul.eu/en/bushings-and-cable-glands/1159-gland-for-pg7-cables-white?SubmitCurrency=1&id_currency=4)
- [1x PG9 cable gland for electrical power cable](https://www.ampul.eu/en/bushings-and-cable-glands/1160-gland-for-pg9-cables-white?SubmitCurrency=1&id_currency=4)
- [1x RPI 4](https://www.dhm-online.com/en/cards-raspberry-pi/7785-raspberry-pi-4-computer-model-b-2gb-ram.html?utm_campaign=pro&utm_source=gmc&utm_medium=gs)
- [1x RPI Power Supply](https://www.ptrobotics.com/fontes-de-alimentacao/7263-raspberry-pi-usb-c-power-supply-51v-3a-branco.html)
- [1x CNC SHIELD](https://www.ptrobotics.com/impressao-3d/5828-cnc-shield-expansion-board.html?gclid=Cj0KCQiAybaRBhDtARIsAIEG3kn42HznvmC32LBnLv7Em2Wc2rBAuW_lVsAoj-hW-7Fsw0lOQkKcui8aAm00EALw_wcB)
- [1x 9V Power Supply for CNC Shield](https://tweakable-parts.com/en/power-supply/476-9v-power-supply-1a.html)
- [1x Arduino Uno R3](https://www.ptrobotics.com/arduino/1033-arduino-uno-r3.html)
- [1x Arduino Cable](https://www.electrofun.pt/cabos-condutores/cabo-usb-arduino-3m)
- [2x A4988 StepperMotor Drivers](https://www.ptrobotics.com/impressao-3d/5829-a4988-stepper-motor-driver-module-with-heatsink-for-3d-printer-impressora-3d.html )
- [2x MicroSwitches](https://www.ptrobotics.com/microswitch/5335-microswitch-5a-roller-verde.html?gclid=Cj0KCQiAybaRBhDtARIsAIEG3knCoLgRckVP7IdKIkZKIxVI3TjjuaLPHm1HIc3Ifo_HjlPLzp0ZvXoaAmdNEALw_wcB)
- [2x Nema 17 Bipolar 1.8deg 60Ncm (85oz.in) 0.64A 10V 42x42x60mm 4 Wires](https://www.omc-stepperonline.com/nema-17-bipolar-1-8deg-60ncm-85oz-in-0-64a-10v-42x42x60mm-4-wires-17hs24-0644s)



**NOTE:** You can buy other antennas, and can also build your own, for that click on this [link](https://satnogs.org/documentation/projects/) 

_Brasil_

    - Omnid



## Software

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/juliosantos99/alma-segment.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/juliosantos99/alma-segment/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
